package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;

@Entity
@Table(name="productos")
public class Productos implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 20 )
	@NotBlank(message = "codigo no puede ir vacio")
	private String codigo;

	@NotNull
	@Column(nullable = false, length = 25 )
	@NotBlank(message = "nombre no puede ir vacio")
	private String nombre;

	@NotNull
	@Column(nullable = false, length = 20 )
	@NotBlank(message = "existencia no puede ir vacio")
	private int existencia;

	@Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaIngreso;

	@Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaModificacion;

	@NotNull
    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCategoria", nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonProperty("idCategoria")
	private Categorias categoria;
	
	@NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idMarca", nullable = false)
    @JsonIdentityReference(alwaysAsId = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonProperty("idMarca")
	private Marcas marca;

	@NotNull
	@Column(nullable = false, length = 20, precision = 2 )
	@NotBlank(message = "precioCompra no puede ir vacio")
	private double precioCompra;

	@NotNull
	@Column(nullable = false, length = 20, precision = 2)
	@NotBlank(message = "precioVenta no puede ir vacio")
	private double precioVenta;

	@NotNull
	@Column(nullable = false, length = 20 )
	@NotBlank(message = "presentacion no puede ir vacio")
	private String presentacion;
	
	@NotNull
    @Column(nullable = false, length = 10 )
    @NotBlank(message = "descuento no puede ir vacio")
    private Double descuento;

	public Productos() {
	}
	
	public Productos(Long id,  String codigo,String nombre, int existencia, Date fechaIngreso, Date fechaModificacion,
			Categorias categoria, Marcas marca, double precioCompra, double precioVenta, String presentacion) {
		this.id = id;
		this.codigo = codigo;
		this.nombre = nombre;
		this.existencia = existencia;
		this.fechaIngreso = fechaIngreso;
		this.fechaModificacion = fechaModificacion;
		this.categoria = categoria;
		this.marca = marca;
		this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
		this.presentacion = presentacion;
	}
	
	public Productos(String codigo,String nombre, int existencia, Date fechaIngreso, Date fechaModificacion,
			Categorias categoria, Marcas marca, double precioCompra, double precioVenta, String presentacion) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.existencia = existencia;
		this.fechaIngreso = fechaIngreso;
		this.fechaModificacion = fechaModificacion;
		this.categoria = categoria;
		this.marca = marca;
		this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
		this.presentacion = presentacion;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public int getExistencia() {
		return existencia;
	}

	public void setExistencia(int existencia) {
		this.existencia = existencia;
	}

	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public Categorias getCategoria() {
		return categoria;
	}
	
	@JsonProperty("idCategoria")
    public void setCategoriaById(Long id) {
	  categoria = Categorias.fromId(id);
    }
	
	@JsonIgnore
	public void setCategoria(Categorias categoria) {
		this.categoria = categoria;
	}

	public Marcas getMarca() {
		return marca;
	}
	
	@JsonProperty("idMarca")
    public void setMarcaById(Long id) {
	  marca = Marcas.fromId(id);
    }
	
	@JsonIgnore
	public void setMarca(Marcas marca) {
		this.marca = marca;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public String getPresentacion() {
		return this.presentacion;
	}

	public void setPresentacion(String presentacion) {
		this.presentacion = presentacion;
	}
	
	public static Productos fromId(Long id) {
	    Productos entity = new Productos();
	    entity.id = id;
	    return entity;
	}
	
	public Double getDescuento() {
        return this.descuento;
    }
	
	public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

}