package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the detallecompra database table.
 * 
 */
@Entity
@Table(name = "detallecompra")
public class Detallecompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 11)
	@NotBlank(message = "cantidad no puede ir vacio")
	private int cantidad;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idProducto", nullable = false)
	@JsonIgnore
	private Productos producto;

	@NotNull
	@Column(nullable = false, precision = 2)
	private double precioCompra;

	@JoinColumn(name = "compra_id")
	@ManyToOne(cascade = CascadeType.REMOVE, optional = false, fetch = FetchType.EAGER)
	private Compras compras;

	@NotNull
	@Column(nullable = false, length = 10)
	private Double descuento;

	public Detallecompra() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public Compras getCompras() {
		return this.compras;
	}

	public void setCompras(Compras compras) {
		this.compras = compras;
	}

	public Double getDescuento() {
		return this.descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}
}