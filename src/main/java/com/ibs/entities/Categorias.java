package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="categorias")
public class Categorias implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;
	
	@NotNull
	@Column(nullable = false, length = 25 )
	@NotBlank(message = "categoria No puede ir vacio")
	private String categoria;

	@NotNull
	@Column(nullable = false, length = 25 )
	@NotBlank(message = "codigo No puede ir vacio")
	private String codigo;

	public Categorias(String categoria,String codigo) {
		this.categoria = categoria;
		this.codigo = codigo;
	}

	public Categorias(Long id, String categoria, String codigo) {
		this.id = id;
		this.categoria = categoria;
		this.codigo = codigo;
	}

	public Categorias() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public static Categorias fromId(Long id) {
      Categorias entity = new Categorias();
      entity.id = id;
      return entity;
  }

}