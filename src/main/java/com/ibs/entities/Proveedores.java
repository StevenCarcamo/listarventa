package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "proveedores", uniqueConstraints = @UniqueConstraint(columnNames = { "dui" }))
public class Proveedores implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 35)
	@NotBlank(message = "correo no puede ir vacio")
	private String correo;

	@NotNull
	@Column(nullable = false, length = 35)
	@NotBlank(message = "direccion no puede ir vacio")
	private String direccion;

	@NotNull
	@Column(nullable = false, length = 10)
	@NotBlank(message = "dui no puede ir vacio")
	private String dui;

	@NotNull
	@Column(nullable = false, length = 8 )
	private int telefono;

	@NotNull
	@Column(nullable = false, length = 20)
	@NotBlank(message = "username no puede ir vacio")
	private String username;

	public Proveedores() {
	}

	public Proveedores(Long id) {
		this.id = id;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDui() {
		return this.dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public int getTelefono() {
		return this.telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}