package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "clientes", uniqueConstraints = @UniqueConstraint(columnNames = { "nit", "dui" }))
public class Clientes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(nullable = false, length = 25)
	@NotBlank(message = "departamento no puede ir vacio")
	private String departamento;

	@NotNull
	@Column(nullable = false, length = 25)
	@NotBlank(message = "direccion no puede ir vacio")
	private String direccion;

	@NotNull
	@Column(nullable = false, length = 10)
	@NotBlank(message = "dui no puede ir vacio")
	private String dui;

	@Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaIngreso;

	@Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaModificacion;

	@NotNull
	@Column(nullable = false, length = 17)
	@NotBlank(message = "giro no puede ir vacio")
	private String giro;

	@NotNull
	@Column(nullable = false, length = 17)
	@NotBlank(message = "nit no puede ir vacio")
	private String nit;

	@NotNull
	@Column(nullable = false, length = 15)
	@NotBlank(message = "nrc no puede ir vacio")
	private String nrc;

	@NotNull
	@Column(nullable = false, length = 8)
	private int telefono;

	@NotNull
	@Column(nullable = false, length = 20)
	@NotBlank(message = "username no puede ir vacio")
	private String username;

	// bi-directional many-to-one association to Venta
	@OneToMany(mappedBy = "cliente")
	@JsonIgnore
	private Set<Ventas> ventas;

	public Clientes() {
	}

	public Clientes(Long idCliente) {
	    this.id = idCliente;
	}

  public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartamento() {
		return this.departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDui() {
		return this.dui;
	}

	public void setDui(String dui) {
		this.dui = dui;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getGiro() {
		return this.giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public String getNit() {
		return this.nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getNrc() {
		return this.nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public int getTelefono() {
		return this.telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Set<Ventas> getVentas() {
		return this.ventas;
	}

	public void setVentas(Set<Ventas> ventas) {
		this.ventas = ventas;
	}

	public Ventas addVenta(Ventas venta) {
		getVentas().add(venta);
		venta.setCliente(this);

		return venta;
	}

	public Ventas removeVenta(Ventas venta) {
		getVentas().remove(venta);
		venta.setCliente(null);

		return venta;
	}

	public static @NotNull Clientes fromId(Long id) {
		Clientes entity = new Clientes();
		entity.id = id;
		return entity;
	}

}