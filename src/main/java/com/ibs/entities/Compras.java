package com.ibs.entities;

import java.io.Serializable;
import java.text.DecimalFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="compras")
public class Compras implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum TipoCompra {
		Gravadas, Exentas;
	}
	
	transient
	DecimalFormat definirDecimal = new DecimalFormat("0.00");

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@NotNull
	@Column(name="totalCompra", nullable = false, length = 20  )
	@NotBlank(message = "username no puede ir vacio")
	private double totalCompra;

	@Column(nullable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaIngreso;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "compras")
	private List<Detallecompra> detalleCompra;

	@NotNull
	@NotBlank(message = "idProveedor no puede ir vacio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idProveedor", nullable = false)
	private Proveedores proveedor;
	
	@NotNull
	@NotBlank(message = "idUsuario no puede ir vacio")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUsuario", nullable = false)
	private Usuarios usuario;

	@NotNull
	@Column(nullable = false, length = 20 )
	@NotBlank(message = "numeroFactura no puede ir vacio")
	private int numeroFactura;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 30 ,columnDefinition = "ENUM('Gravadas', 'Exentas')")
	private TipoCompra tipoCompra;

	public Compras() {
	}

	public Compras(Long id, Double totalCompra, List<Detallecompra> detalleCompra, TipoCompra tipoCompra,
			Proveedores proveedor, int numeroFactura) {
		this.id = id;
		this.numeroFactura = numeroFactura;
		this.tipoCompra = tipoCompra;
		this.totalCompra = totalCompra;
		this.proveedor = proveedor;
		this.detalleCompra = detalleCompra;
	}

	public Compras(Double totalCompra, List<Detallecompra> detalleCompra, TipoCompra tipoCompra,
			Proveedores proveedor, int numeroFactura) {
		this.numeroFactura = numeroFactura;
		this.tipoCompra = tipoCompra;
		this.totalCompra = totalCompra;
		this.proveedor = proveedor;
		this.detalleCompra = detalleCompra;
	}


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getTotalCompra() {
	    String numero = definirDecimal.format(totalCompra);
	    totalCompra = Double.valueOf(numero);
		return totalCompra;
	}

	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}

	public Date getFechaIngreso() {
		return this.fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public Proveedores getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedores proveedor) {
		this.proveedor = proveedor;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}

	public int getNumeroFactura() {
		return this.numeroFactura;
	}

	public void setNumeroFactura(int numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public TipoCompra getTipoCompra() {
		return tipoCompra;
	}

	public void setTipoCompra(TipoCompra tipoCompra) {
		this.tipoCompra = tipoCompra;
	}

	public List<Detallecompra> getDetalleCompra() {
		return this.detalleCompra;
	}

	public void setDetalleCompra(List<Detallecompra> detalleCompra) {
		this.detalleCompra = detalleCompra;
	}
}