package com.ibs.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.ibs.entities.Productos;
import com.ibs.repository.ProductosRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductosService {

    ProductosRepository repository;

    @Autowired
    public ProductosService(ProductosRepository repository) {
        this.repository = repository;
    }

    public Object getDatos() {
        String iconoEditar = "<i class='fas fa-edit m-1' style='color: black'></i> <strong style='color: black'></strong>";
        String iconoEliminar = "<i class='fas fa-trash-alt m-1' style='color: black'></i> <strong style='color: black'></strong>";
        String iconoVer = "<i class='fas fa-eye m-1'></i> <strong style='color: black'></strong>";

        String iconoSeleccionar = "<i class='fa fa-plus m-1'></i> <strong style='color: black'></strong>";

        List<HashMap<String, Object>> registros = new ArrayList<>();
        List<Productos> lista = (List<Productos>) repository.findAll();

        for (Productos entity : lista) {
            HashMap<String, Object> object = new HashMap<>();
 
            object.put("id", entity.getId());
            object.put("nombre", entity.getNombre());
            object.put("codigo", entity.getCodigo());
            object.put("existencia", entity.getExistencia());
            object.put("fechaIngreso", entity.getFechaIngreso());
            object.put("fechaModificacion", entity.getFechaModificacion());
            object.put("precioCompra", entity.getPrecioCompra());
            object.put("precioVenta", entity.getPrecioVenta());
            object.put("presentacion", entity.getPresentacion());
            object.put("descuento", entity.getDescuento());
            object.put("categoria", entity.getCategoria().getCategoria());
            object.put("marca", entity.getMarca().getMarca());

            object.put("seleccionar",
                    "<button type='button' data-toggle='tooltip' data-placement='top' title='Ver' data-toggle='modal' data-target='#ver' class='btn btn-primary  ml-3 mt-1'"
                            + "onclick='seleccionarProducto(" + entity.getId() + ")'> " + iconoSeleccionar
                            + "</button>");

            object.put("operacion",
                    "<button type='button' data-toggle='modal' data-target='#ver' class='btn btn-success ml-3 mt-1'"
                            + "onclick='cargarRegistro2(" + entity.getId() + ")'> " + iconoVer + "</button>"
                            + "<button type='button' data-toggle='modal' data-target='#editar' class='btn btn-warning ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEditar + "</button>"
                            + "<button type='button' data-toggle='modal' data-target='#eliminar' class='btn btn-danger ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEliminar + "</button>");

            registros.add(object);
        }
        return Collections.singletonMap("data", registros);
    }

}