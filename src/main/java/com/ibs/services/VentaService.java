package com.ibs.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibs.entities.Clientes;
import com.ibs.entities.Productos;
import com.ibs.entities.Usuarios;
import com.ibs.entities.Ventas;
import com.ibs.repository.ClientesRepository;
import com.ibs.repository.ProductoRepository;
import com.ibs.repository.UsuarioRepository;
import com.ibs.repository.VentaRepository;

@Service
public class VentaService {

  @Autowired
  ProductoRepository rProducto;

  @Autowired
  ClientesRepository rCliente;
  
  @Autowired
  UsuarioRepository rUser;

  @Autowired
  VentaRepository rVenta;

  @Transactional
  public List<Ventas> getAllVenta() {
      return (List<Ventas>) rVenta.findAll();
  }

  @Transactional
  public Boolean save(Ventas entity) {
      try {
          rVenta.save(entity);
          return true;
      } catch (Exception e) {
          System.err.println("Error: " + e.getMessage());
          return false;
      }
  }

  @Transactional
  public List<Clientes> getAllClientes() {
      return (List<Clientes>) rCliente.findAll();
  }

  @Transactional
  public List<Productos> getAllProductos() {
      return (List<Productos>) rProducto.findAll();
  }

  @Transactional
  public Clientes getCliente(Long id) {
      return rCliente.findById(id).get();
  }
  
  @Transactional
  public Usuarios getUsuario(Long id) {
    return rUser.findById(id).get();
  }
  

  @Transactional
  public Productos getProducto(Long id) {
      return rProducto.findById(id).get();
  }

}
