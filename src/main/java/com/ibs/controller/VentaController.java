package com.ibs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.ibs.entities.Ventas;
import com.ibs.entities.Detalleventa;
import com.ibs.entities.Ventas.TipoVenta;
import com.ibs.repository.ClientesRepository;
import com.ibs.repository.ProductosRepository;
import com.ibs.repository.VentaRepository;
import com.ibs.services.VentaService;

@Controller
@RequestMapping("/venta/")
@CrossOrigin
public class VentaController {

    @Autowired
    VentaService sVenta;

    @Autowired
    VentaRepository repository;

    @Autowired
    ProductosRepository ipr;

    @Autowired
    ClientesRepository icr;

    public static List<Detalleventa> detalles = new ArrayList<Detalleventa>();

    @GetMapping(value = "agregarDetalle")
    @ResponseBody
    @CrossOrigin
    public Detalleventa agregarDetalle(@RequestParam final int cantidad, @RequestParam final Long producto) {

        final Detalleventa vp = new Detalleventa();
        vp.setCantidad(cantidad);
        vp.setProducto(sVenta.getProducto(producto));
        detalles.add(vp);
        return vp;
    }

    @GetMapping(value = "allDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public List<Detalleventa> getDetallesMemoria() {
        return detalles;
    }

    @GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getDetalles() {
        return detalles;
    }

   @GetMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object resetDetalles() {
        detalles = new ArrayList<>();
        // detalles.clear();
        return "lista reseteada";
    }

    @GetMapping(value = "save")
    @ResponseBody
    @CrossOrigin
    public Boolean save(@RequestParam final int numeroFactura, @RequestParam final TipoVenta tipoVenta,
            @RequestParam final Long idCliente, @RequestParam final Long idUsuario) {

        final Ventas entity = new Ventas();

        entity.setNumeroFactura(numeroFactura);
        entity.setTipoVenta(tipoVenta);
        entity.setUsuario(sVenta.getUsuario(idUsuario));
        entity.setCliente(sVenta.getCliente(idCliente));

        for (final Detalleventa detalleVenta : detalles) {
            detalleVenta.setVentas(entity);
        }
        System.out.println(detalles + "" + idCliente);

        entity.setDetalleVenta(detalles);

        try {
            sVenta.save(entity);
            return true;
        } catch (final Exception e) {
            return false;
        }        
    }

    @GetMapping( value ="ventas",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transactional
    public Object getdatos(){
        String iconoEditar = "<i class='fas fa-edit'></i> <strong >Editar</strong>";
        String iconoEliminar = "<i class='fas fa-trash-alt text-black'></i> <strong >Eliminar</strong>";

        final List<HashMap<String,Object>> registros = new ArrayList<>();
        final List<Ventas> lista=(List<Ventas>) sVenta.getAllVenta();

        for(final Ventas entity :lista){
            final HashMap<String,Object> object = new HashMap<>();

            object.put("id",entity.getId());
            object.put("fecha_venta",entity.getFechaVenta());
            object.put("nombre",entity.getNombre());
            object.put("numero_factura",entity.getNumeroFactura());
            object.put("tipo_venta",entity.getTipoVenta());
            object.put("total_venta",entity.getTotalVenta());
            object.put("cliente",entity.getCliente().getUsername());
            object.put("usuario",entity.getUsuario().getUsername());
            object.put("operaciones",
                    "<button type='button' data-toggle='modal' data-target='#' class='text-black btn btn-warning ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEditar + "</button>"
                            + "<button type='button' data-toggle='modal' data-target='#' style='color: black' class=' btn btn-danger ml-3 mt-1'"
                            + "onclick='cargarRegistro(" + entity.getId() + ")'> " + iconoEliminar + "</button>");
            registros.add(object);
        }
        return Collections.singletonMap("data", registros);
    }
}