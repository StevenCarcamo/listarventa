package com.ibs.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.transaction.Transactional;

import com.ibs.entities.Compras;
import com.ibs.entities.Detallecompra;
import com.ibs.entities.Productos;
import com.ibs.entities.Proveedores;
import com.ibs.entities.Compras.TipoCompra;
import com.ibs.repository.ProductosRepository;
import com.ibs.repository.ProveedoresRepository;
import com.ibs.services.CompraService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * CompraController
 */
@Controller
@RequestMapping("/compra")
public class CompraController {

    @Autowired
    CompraService sCompra;

    @Autowired
    ProductosRepository ipr;

    @Autowired
    ProveedoresRepository icr;

    public static List<Detallecompra> detalles = new ArrayList<Detallecompra>();

    @GetMapping
    public String guardarMostrar(final Model model, @RequestParam(required = false) final Proveedores proveedor) {
        model.addAttribute("productos", sCompra.getAllProductos());
        model.addAttribute("proveedor", sCompra.getAllProveedores());

        detalles = new ArrayList<Detallecompra>();

        return new String("vistas/Compras/agregar");
    }

    @GetMapping(value = "/api/proveedores", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Compras> indexJSON() {
        return sCompra.getAllCompra();
    }

    @GetMapping(value = "api/productos")
    @ResponseBody
    public List<Productos> productosJSON() {
        return sCompra.getAllProductos();
    }

    @GetMapping(value = "getProveedores", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transactional
    public Object getProveedor() {
        final List<HashMap<String, Object>> registros = new ArrayList<>();
        final List<Proveedores> l = (List<Proveedores>) icr.findAll();

        for (final Proveedores entity : l) {
            final HashMap<String, Object> object = new HashMap<>();

            object.put("id", entity.getId());
            object.put("nombre", entity.getUsername());
            object.put("direccion", entity.getDireccion());
            object.put("dui", entity.getDui());
            object.put("telefono", entity.getTelefono());
            object.put("correo", entity.getCorreo());

            object.put("operacion",
                    "<button type='button' class= 'btn btn-primary agregarProveedor' data-dismiss='modal'>AGREGAR</button>");
            registros.add(object);
        }
        return Collections.singletonMap("data", registros);

    }

    @GetMapping(value = "getProductos", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transactional
    public Object getProducto() {
        final List<HashMap<String, Object>> registros = new ArrayList<>();
        final List<Productos> l = (List<Productos>) ipr.findAll();

        for (final Productos producto : l) {
            final HashMap<String, Object> object = new HashMap<>();

            object.put("id", producto.getId());
            object.put("codigo", producto.getCodigo());
            object.put("nombre", producto.getNombre());
            object.put("presentacion", producto.getPresentacion());
            object.put("existencia", producto.getExistencia());
            object.put("categoria", producto.getCategoria().getCategoria());
            object.put("marca", producto.getMarca().getMarca());

            object.put("operacion",
                    "<button type='button' class= 'btn btn-primary agregarProducto' data-dismiss='modal'>AGREGAR</button>");
            registros.add(object);
        }
        return Collections.singletonMap("data", registros);
    }

    @GetMapping(value = "agregarDetalle")
    @ResponseBody
    @CrossOrigin
    public Detallecompra agregarDetalle(@RequestParam int cantidad, @RequestParam Long idProducto,
            @RequestParam Double descuento, @RequestParam Double precioCompra) {

        final Detallecompra detalleC = new Detallecompra();
        detalleC.setCantidad(cantidad);
        detalleC.setDescuento(descuento);
        detalleC.setPrecioCompra(precioCompra);
        //detalleC.setProducto(sCompra.getProducto((long)2));
        detalles.add(detalleC);
        return detalleC;
    }

    @GetMapping(value = "allDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @CrossOrigin
    public List<Detallecompra> getDetallesMemoria() {
        return detalles;
    }

    @GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getDetalles() {
        return detalles;
    }

    @GetMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object resetDetalles() {
        detalles = new ArrayList<>();
        // detalles.clear();
        return "lista reseteada";
    }

    @GetMapping(value = "save")
    @ResponseBody
    @CrossOrigin
    public Boolean save(@RequestParam int numeroFactura, @RequestParam TipoCompra tipoVenta,
            @RequestParam Long idProveedor, @RequestParam Long idUsuario) {

        Compras entity = new Compras();

        entity.setNumeroFactura(numeroFactura);
        entity.setTipoCompra(tipoVenta);
        entity.setUsuario(sCompra.getUsuario(idUsuario));
        entity.setProveedor(sCompra.getProveedores(idProveedor));

        for (Detallecompra detalleVenta : detalles) {
            detalleVenta.setCompras(entity);
        }
        System.out.println(detalles + "" + idProveedor);

        entity.setDetalleCompra(detalles);

        try {
            sCompra.save(entity);
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}