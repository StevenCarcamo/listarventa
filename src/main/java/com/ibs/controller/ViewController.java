package com.ibs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.ibs.repository.RoleRepository;

@Controller
public class ViewController {

  @Autowired
  RoleRepository rpRoles;

  /** Redirige_vista_listar */
  @GetMapping(value = "roles")
  public String roles() {
    return "vistas/Rol/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "marcas")
  public String marcas() {
    return "vistas/Marcas/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "categorias")
  public String categorias() {
    return "vistas/Categorias/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "usuarios")
  public String user(ModelMap mp) {
    mp.put("list", rpRoles.findAll());
    return "vistas/User/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "proveedores")
  public String proveedores() {
    return "vistas/Proveedores/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "inventarios")
  public String inventario() {
    return "vistas/Inventario/Listar";
  }

  @GetMapping(value = "clientes")
  public String cliente() {
    return "vistas/Clientes/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "productos")
  public String productos() {
    return "vistas/Productos/Listar";
  }

  @GetMapping(value = "ventas")
  public String nuevaVenta() {
    return "vistas/Ventas/agregarVenta";
  }
  //redirige al listado de las ventas
  @GetMapping(value = "listado")
  public String listadoVenta() {
    return "vistas/Ventas/listadoventas";
  }

  @GetMapping(value = "compras")
  public String nuevaCompra() {
    return "vistas/Compras/agregarCompra";
  }
}
