package com.ibs.controller;

import java.util.ArrayList;
import java.util.List;

import com.ibs.entities.Detalleventa;
import com.ibs.services.VentaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/ventas/")
@CrossOrigin
public class VentaApi {

    @Autowired
    VentaService sVenta;

    public static List<Detalleventa> detalles = new ArrayList<Detalleventa>();

    @GetMapping(value = "agregarDetalle")
    @ResponseBody
    public Detalleventa agregarDetalle(@RequestParam int cantidad, @RequestParam Long producto) {

        System.out.println("******************************************"+producto);
        final Detalleventa vp = new Detalleventa();
        vp.setCantidad(cantidad);
        vp.setProducto(sVenta.getProducto(producto));
        detalles.add(vp);
        return vp;
    }
	
	  @GetMapping(value = "allDetalles", produces =
	  MediaType.APPLICATION_JSON_VALUE)
	  
	  @ResponseBody
	  
	  @CrossOrigin public List<Detalleventa> getDetallesMemoria() { return
	  detalles; }
	 

    @GetMapping(value = "detalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object getDetalles() {
        return detalles;
    }

    @GetMapping(value = "resetDetalles", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object resetDetalles() {
        detalles = new ArrayList<>();
        // detalles.clear();
        return "lista reseteada";
    }

}