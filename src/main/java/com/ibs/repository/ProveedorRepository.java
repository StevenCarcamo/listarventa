package com.ibs.repository;

import com.ibs.entities.Proveedores;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * ProveedorRepository
 */
@Repository
public interface ProveedorRepository extends CrudRepository<Proveedores, Long>{

    
}