package com.ibs.repository;

import com.ibs.entities.Productos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * ProductoRepository
 */
@Repository
public interface ProductoRepository extends CrudRepository<Productos, Long>{

    
}