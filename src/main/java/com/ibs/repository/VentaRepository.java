package com.ibs.repository;

import com.ibs.entities.Ventas;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * VentaRepository
 */
@Repository
public interface VentaRepository extends CrudRepository<Ventas, Long>{

}