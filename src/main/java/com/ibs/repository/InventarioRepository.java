package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Inventario;

@Repository
public interface InventarioRepository extends CrudRepository<Inventario, Long> {

}
