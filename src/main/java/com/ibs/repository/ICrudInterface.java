package com.ibs.repository;

import java.util.List;
import java.util.Optional;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;

import com.ibs.entities.Usuarios;

public interface ICrudInterface<T, ID> {

    /**
     * Save an entity
     * 
     * @param entity - The entity to be saved
     * @return The entity saved
     * 
     * @author thanks creation by roman.gonzalez
     */
    T save(T entity);

    /**
     * Update an entity
     * 
     * @param entity - The entity to be updated
     * @return The entity updated
     * @throws EntityNotFoundException - Exception to be throw if entity doesn't
     *                                 exist
     * 
     */
    T update(T entity, ID id);

    /**
     * Delete entity by id
     * 
     * @param id - The entity id
     * @return A boolean indicating if the entity was deleted
     * @throws EntityNotFoundException - Exception to be throw if entity doesn't
     *                                 exist
     * 
     */
    Boolean deleteById(ID id);

    /**
     * Find entity by id
     * 
     * @param id - the entity id
     * @return The found entity
     * @throws EntityNotFoundException - Exception to be throw if entity doesn't
     *                                 exist
     * 
     */
    Optional<T> getById(ID id);

    /**
     * Get all entities
     * 
     * @return All found entities
     * 
     */
    List<T> getAlls();

    Usuarios getAllByEmail(Authentication authentication);

    public Object getDatos();

}