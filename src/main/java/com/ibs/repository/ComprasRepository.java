package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Compras;

@Repository
public interface ComprasRepository extends CrudRepository<Compras, Long> {

}
