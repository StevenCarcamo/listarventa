package com.ibs.repository;

import com.ibs.entities.Usuarios;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * UsuarioRepository
 */
@Repository
public interface UsuarioRepository extends CrudRepository<Usuarios, Long>{

    
}