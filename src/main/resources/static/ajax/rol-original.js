$(document).ready(inicio);
let rol = {
	id : 0
};

function setIdRol(id) {
	rol.id = id;
}

// Cargando las funciones
function inicio() {
	cargarDatos();
	$("#btnGuardar").click(guardar);
	$("#btnEliminar").click(function() {
		eliminar(rol.id);
	});
	$("#btnActualizar").click(modificar);
}

// Funcion para resetear inputs
function reset() {
	$("#id").val(null);
	$("#rol").val(null);

	$("#id2").val(null);
	$("#rol2").val(null);
}

// Funcion para cargar datos en tabla
*function cargarDatos() {
	$.ajax({
		url : api,
		method : "Get",
		success : function(response) {
			$("#tdatos").html("");
			
			var modalEditar = "data-toggle='modal' data-target='#editar' class='btn btn-warning ml-3 mt-1 ' > <i class='fas fa-edit'></i> ";
			var modalEliminar = "data-toggle='modal' data-target='#eliminar' class='btn btn-danger ml-3 mt-1'> <i class='fas fa-trash-alt'></i> ";
			
			for (let i = 0; i < response.length; i++) {
				
				$("#tdatos").append(
					"<tr>" +
					"<td>" + response[i].id + "</td>" +
					"<td>" +  response[i].rol + "</td>" +
					"<td>" + 
						"<button onclick='cargarRegistro(" + response[i].id + ");'" +modalEditar+ " Editar</button>" +
						"<button onclick='cargarRegistro(" + response[i].id + ");'" +modalEliminar+ " Eliminar</button>" +
					"</td> </tr>");
					}
				},
				error : function(response) {
					alert("Eror en la peticion: " + response);
				}
			});

}

// Funcion para enviar data de nuevo registro y guardar
function guardar() {
	$.ajax({
		url : "/rol/saveOrUpdate",
		method : "Post",
		data : {
			id : null, // PARA GUARDAR EL id TIENE QUE IR NULO
			rol : $("#rol").val()
		},
		success : function(response) {
			reset();
			cargarDatos();
		},
		error : function(response) {
			alert("El registro no puede ir vacio ");
		}
	});
}

// Funcion para eliminar registro
function eliminar(id) {
	$.ajax({
		url : "/rol/delete/" + id,
		method : "delete",
		success : function(response) {
			cargarDatos();
		},
		error : function(response) {
			alert("Error en la peticion " + response);
		}
	});
}

// Funcion para cargar registro en modal editar
function cargarRegistro(id) {
	$.ajax({
		url : "/rol/update/" + id,
		method : "Get",
		success : function(response) {
			$("#id2").val(response.idRol);
			$("#rol2").val(response.nombreRol);
		},
		error : function(response) {
			alert("Error " + response);
		}
	});
}


// Funcion para modificar registro
function modificar() {
	$.ajax({
		url : "/rol/saveOrUpdate",
		method : "Post",
		data : {
			id : $("#id2").val(),
			rol : $("#rol2").val(),
		},
		success : function(response) {
			cargarDatos();
			reset();
		},
		error : function(response) {
			alert("Error en la peticion " + response);
		}
	});
}

// Funcion para Data Table
$(document).ready(function () {
	var table = $("#table").DataTable ({
		
	});
});