$(document).ready(inicio);
const api = "/inventario/";

//busca_id
function cargarRegistro(id) {
	$('.btnload').text('Modificar');
	$('.tituloModal').text('Modificar Registro');
	$.ajax({
		url : api + id,
		method : "post",
		success : function(response) {
			$("#id").val(response.id);
			$("#idProducto").val(response.idProducto);
			$("#evento").val(response.evento);
			$("#cantidad").val(response.cantidad);
			$("#descripcion").val(response.descripcion);
			
			//llenar el modal eliminars
			$("#dato").text(response.idProducto);
			setId(response.id);
		},
		error : function(response) {
			alert("Error al obtener el dato");
		}
	});
}

//funcion_listar
function cargarDatos() {
    $("#tablaConsultas").DataTable({

        "ajax": {
            "url": api,
            "method": "post"
        },
        "columns": [{
                "data": "id",
                "width": "5%"
            },
            {
            	"data": "producto",
            	"width": "20%"
            },
            {
                "data": "evento",
                "width": "20%"
            },
            {
            	"data": "cantidad",
            	"width": "20%"
            },
            {
            	"data": "descripcion",
            	"width": "20%"
            },
            {
                "data": "operacion",
                "width": "5%"
            }
        ],
        
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}
