let proveedores = {
    id: 0,
    nombre: ""
};

let productos = {
    id: 0,
    nombre: ""
};

/* funcion para cargar los elementos llamados */
$(document).ready(function () {
    cargarProveedores();
    cargarProductos();
    //$("#guardarCompra").click(guardar);

    $("#agregarDetalle").click(agregar);

    $("body").on('click', '.agregarProveedor', function () {
        agregarProveedor($(this).parent().parent().children('td:eq(0)').text(), $(this)
            .parent()
            .parent().children('td:eq(1)').text());
    });

    $("body").on('click', '.agregarProducto', function () {

        agregarProducto($(this).parent().parent().children('td:eq(0)').text(),
            $(this).parent().parent().children('td:eq(2)').text()
        );
    });

});
/* Boton de agregar cliente en la tabla */

function agregarProveedor(id, nombre) {
    proveedores.id = id;
    proveedores.nombre = nombre;

    $("#proveedor").val(proveedores.nombre);
}

function agregarProducto(id, nombre) {
    productos.id = id;
    productos.nombre = nombre;

    $("#idProducto").val(productos.nombre);
}

function cargarProveedores() {
    $("#tablaProveedores").DataTable({
        "ajax": {
            "url": "/compra/getProveedores",
            "method": "Get"
        },
        "columns": [{
                "data": "id",
                "width": "5%"
            },
            {
                "data": "nombre",
                "width": "10%"
            },
            {
                "data": "direccion",
                "width": "10%"
            },
            {
                "data": "dui",
                "width": "30%"
            },
            {
                "data": "telefono",
                "width": "10%"
            },
            {
                "data": "correo",
                "width": "10%"
            },
            {
                "data": "operacion",
                "width": "5%"
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
}

function cargarProductos() {
    $("#tablaProductos").DataTable({
        "ajax": {
            "url": "/compra/getProductos",
            "method": "Get"
        },
        "columns": [{
                "data": "id",
            },
            {
                "data": "codigo",
            },
            {
                "data": "nombre",
            },
            {
                "data": "presentacion",
            },
            {
                "data": "existencia",
            },
            {
                "data": "categoria",
            },
            {
                "data": "marca",
            },
            {
                "data": "operacion",
            }
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
}

function agregar() {
    var desc = $("#descuento").val() / 100;
    var producto = productos.id;

    $.ajax({
        url: "/compra/agregarDetalle",
        method: "Get",
        data: {
            cantidad: $("#cantidad").val(),
            descuento: desc,
            idProducto: producto,
            precioCompra: $("#precioCompra").val()
        },
        success: function (response) {

            console.log(response);
            console.log(productos.id);

            alert("Detalle agregado");
            $("#cantidad").val(0);
            $("#descuento").val(0);
            $("#producto").val("");
            $("#precioCompra").val("");
            productos.id = 0;
            productos.nombre = "";
            cargarDetalles();
        },
        error: function (response) {
            alert("NO SE AGREGO EL DETALLE");
        }
    });
}

function cargarDetalles() {
    $.ajax({
        url: "/compra/allDetalles",
        method: "Get",
        success: function (response) {
            $("#tDetalles").html("");
            console.log(response);
            response.forEach(i => {

                $("#tDetalles").append("" +
                    "<tr>" +
                    "<td>" + i.cantidad + "</td>" +
                    "<td>" + i.idProducto.nombre + "</td>" +
                    "<td>$" + i.idProducto.precioCompra + "</td>" +
                    "<td>" + i.idProducto.descuento + "%</td>" +
                    "<td><button class='btn btn-danger'>eliminar</button></td>" +
                    "</tr>" +
                    "");
            });
        },
        error: function (response) {}
    });
}

function resetDetalles() {
    $.ajax({
        url: "/compra/resetDetalles",
        method: "Get"
    });
}

function guardar() {
    var nf = $("#numeroFactura").val();
    var tv = $("#tipoCompra").val();
    console.log(nf)
    $.ajax({
        url: "/compra/save",
        method: "Get",
        data: {
            idUsuario: 1,
            numeroFactura: nf,
            tipoCompra: tv,
            idCliente: clientes.id
        },
        success: function (response) {

            alert("DETALLE GUARDADO CORRECTAMENTE...");
            location.reload(); //para recargar la página
        },
        error: function (response) {
            alert("DETALLE NO GUARDADO ...");
        }
    })
}