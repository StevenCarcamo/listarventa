	$(document).ready(inicio);
	const api = "/venta/";
	
	function inicio(){
		cargarDatos();
	}

// funcion para cargar datos en la vista de listado de ventas
function cargarDatos() {
	$("#tablaventas").DataTable({
		
		"ajax" : {
			"url" : api + "ventas",
			"method" : "Get"
		},
		"columns" : [/*
						 * { "data": "id", },
						 */
		{
			"data" : "fecha_venta",
		}, {
			"data" : "nombre",
		}, {
			"data" : "numero_factura",
		}, {
			"data" : "tipo_venta",
		}, {
			"data" : "total_venta",
		}, {
			"data" : "cliente",
		}, {
			"data" : "usuario",
		}, {
			"data" : "operaciones",
		} 
		],

		"language" : {
			"lengthMenu" : "Mostrar _MENU_ ",
			"zeroRecords" : "Datos no encontrados",
			"info" : "Mostar páginas _PAGE_ de _PAGES_",
			"infoEmpty" : "Datos no encontrados",
			"infoFiltered" : "(Filtrados por _MAX_ total registros)",
			"search" : "Buscar:",
			"paginate" : {
				"first" : "Primero",
				"last" : "Anterior",
				"next" : "Siguiente",
				"previous" : "Anterior"
			}
		},
		"bDestroy" : "true"
	});
}