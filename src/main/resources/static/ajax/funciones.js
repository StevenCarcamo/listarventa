let model = {
	id : 0
};

function setId(id) {
	model.id = id;
}

// Cargando las funciones
function inicio() {
	cargarDatos();
	$(".btnLoad").click(function (){
		actualizar(event);
	});
	$("#btnEliminar").click(function() {
		eliminar(model.id);
	});
	$(".btnCancelar").click(function(){
		$('.modal').modal('hide')
	});
	$("#nuevo").click(function(){
		$('.tituloModal').text('Nuevo Registro');
		$('.btnload').text('Guardar');
		$('#formUpdate input').focus();
		resetForm($('#formUpdate'));
		$('#editar').modal('show');
		
	});
}

//Mensaje_error
function errorRequest(response){
	$('.msg').text("* No pueden ir campos vacios");
}

//Funcion_peticion_exito
function exito(){
	cargarDatos();
	$('.msg').text("");
	resetForm($('#formUpdate'));
	$('#editar').modal('hide');
}

//reset_opcion_1
function reset(){
  $('#formUpdate input').each(function () {
  	this.value = null;
  });
}
//opcion_2
function resetForm($form) {
  $form.find('input:text,input:hidden, input:password, input:file, textarea').val(''); //si_de_desea_resetear: select
  $form.find('input:radio, input:checkbox')
       .removeAttr('checked').removeAttr('selected');
}
//luego_usar la_id_del form_a_resetear
//resetForm($('#formNew'));

//Funcion_guardar_actualizar
function actualizar(event) {
	event.preventDefault();
	var datos = {};
    $('#formUpdate input, select').each(function () {
    	datos[this.id] = this.value;
    });
    
	$.ajax({
		url: api,
		type: 'put',
		data: JSON.stringify(datos),
		contentType: "application/json",
		error : errorRequest,
	})
	.done(function() { //peticion_exitosa
		exito();
		console.log("exito");
		
	})
}

//Funcion_eliminar
function eliminar(id) {
	$.ajax({
		url: api + id,
		type: 'delete',
		error : errorRequest,
	})
	.done(function() { //peticion_exitosa
		cargarDatos();
		$('.msg').text("");
		$('#eliminar').modal('hide');
		
	})
}